module.exports = function phaseListToTaskList(phasedList) {
    var tasks = [];
  if (phasedList.length == 0) {
    return false;
  }
  var level = -1;
  parentArray = [];
  checkChildren(phasedList, level, "");

  function checkChildren(list, level, parent) {
    level++;
    list.forEach((task) => {
      if (task.children.length == 0) {
        tasks.push({ ...task, level: level, parent: parent });
      } else {
        let children1 = task.children;
        let childrenIDs = [];
        children1.forEach((childIDObject) => {
          childrenIDs.push(childIDObject.index);
        });
        tasks.push({
          ...task,
          children: childrenIDs,
          level: level,
          parent: parent,
        });
        checkChildren(children1, level, task.index);
      }
    });
  }

  return tasks;
}