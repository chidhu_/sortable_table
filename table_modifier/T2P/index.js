module.exports = function taskListToPhasedList(taskList) {
  let output = [];
  let maxLevel = 0;
  taskList.forEach((item) => {
    if (item.level > maxLevel) {
      maxLevel = item.level;
    }
  });
  taskList.forEach((item) => {
    item.children = [];
  });
  let levelItems = [];
  for (i = maxLevel; i >= 0; i--) {
    // current i level tasks
    levelItems = taskList.filter((item) => {
      return item.level == i;
    });
    let levelItemChildren = [];
    levelItems.forEach((item) => {
      let itemParent = item.parent;
      if (i !== 0) {
        let parentObject = taskList.filter((item) => {
          return item.index == itemParent;
        });
        parentObject[0].children.push(item);
        output.push(parentObject);
      } else {
        output.push(item);
      }
    });
  }
  let level0ItemsCount = taskList.filter((item) => {
    return item.level == 0;
  }).length;
  console.log(level0ItemsCount)
  let wantedItems = output.slice(-1 * level0ItemsCount);
  return wantedItems;
};
