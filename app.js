const {
  taskListToPhasedList,
  phaseListToTaskList,
} = require("./table_modifier");
let phasedList = require("./phased.json");
const fs = require("fs");

let phList = phaseListToTaskList(phasedList);
console.log(phList);

let tsList = taskListToPhasedList(phList);
console.log(tsList);

